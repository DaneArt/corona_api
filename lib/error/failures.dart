import 'package:freezed_annotation/freezed_annotation.dart';

part 'failures.freezed.dart';


@freezed
abstract class StorageFailure with _$StorageFailure{

   factory StorageFailure.networkFailure() = NetworkFailure;
   factory StorageFailure.cacheFailure() = CacheFailure;

   @late
   String get message  => this.map(networkFailure: (e)=>"NetworkException", cacheFailure: (e)=> "CacheException.");
}