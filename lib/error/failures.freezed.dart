// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies

part of 'failures.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

/// @nodoc
class _$StorageFailureTearOff {
  const _$StorageFailureTearOff();

// ignore: unused_element
  NetworkFailure networkFailure() {
    return NetworkFailure();
  }

// ignore: unused_element
  CacheFailure cacheFailure() {
    return CacheFailure();
  }
}

/// @nodoc
// ignore: unused_element
const $StorageFailure = _$StorageFailureTearOff();

/// @nodoc
mixin _$StorageFailure {
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result networkFailure(),
    @required Result cacheFailure(),
  });
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result networkFailure(),
    Result cacheFailure(),
    @required Result orElse(),
  });
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result networkFailure(NetworkFailure value),
    @required Result cacheFailure(CacheFailure value),
  });
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result networkFailure(NetworkFailure value),
    Result cacheFailure(CacheFailure value),
    @required Result orElse(),
  });
}

/// @nodoc
abstract class $StorageFailureCopyWith<$Res> {
  factory $StorageFailureCopyWith(
          StorageFailure value, $Res Function(StorageFailure) then) =
      _$StorageFailureCopyWithImpl<$Res>;
}

/// @nodoc
class _$StorageFailureCopyWithImpl<$Res>
    implements $StorageFailureCopyWith<$Res> {
  _$StorageFailureCopyWithImpl(this._value, this._then);

  final StorageFailure _value;
  // ignore: unused_field
  final $Res Function(StorageFailure) _then;
}

/// @nodoc
abstract class $NetworkFailureCopyWith<$Res> {
  factory $NetworkFailureCopyWith(
          NetworkFailure value, $Res Function(NetworkFailure) then) =
      _$NetworkFailureCopyWithImpl<$Res>;
}

/// @nodoc
class _$NetworkFailureCopyWithImpl<$Res>
    extends _$StorageFailureCopyWithImpl<$Res>
    implements $NetworkFailureCopyWith<$Res> {
  _$NetworkFailureCopyWithImpl(
      NetworkFailure _value, $Res Function(NetworkFailure) _then)
      : super(_value, (v) => _then(v as NetworkFailure));

  @override
  NetworkFailure get _value => super._value as NetworkFailure;
}

/// @nodoc
class _$NetworkFailure implements NetworkFailure {
  _$NetworkFailure();

  bool _didmessage = false;
  String _message;

  @override
  String get message {
    if (_didmessage == false) {
      _didmessage = true;
      _message = this.map(
          networkFailure: (e) => "NetworkException",
          cacheFailure: (e) => "CacheException.");
    }
    return _message;
  }

  @override
  String toString() {
    return 'StorageFailure.networkFailure(message: $message)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is NetworkFailure);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result networkFailure(),
    @required Result cacheFailure(),
  }) {
    assert(networkFailure != null);
    assert(cacheFailure != null);
    return networkFailure();
  }

  @override
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result networkFailure(),
    Result cacheFailure(),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (networkFailure != null) {
      return networkFailure();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result networkFailure(NetworkFailure value),
    @required Result cacheFailure(CacheFailure value),
  }) {
    assert(networkFailure != null);
    assert(cacheFailure != null);
    return networkFailure(this);
  }

  @override
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result networkFailure(NetworkFailure value),
    Result cacheFailure(CacheFailure value),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (networkFailure != null) {
      return networkFailure(this);
    }
    return orElse();
  }
}

abstract class NetworkFailure implements StorageFailure {
  factory NetworkFailure() = _$NetworkFailure;
}

/// @nodoc
abstract class $CacheFailureCopyWith<$Res> {
  factory $CacheFailureCopyWith(
          CacheFailure value, $Res Function(CacheFailure) then) =
      _$CacheFailureCopyWithImpl<$Res>;
}

/// @nodoc
class _$CacheFailureCopyWithImpl<$Res>
    extends _$StorageFailureCopyWithImpl<$Res>
    implements $CacheFailureCopyWith<$Res> {
  _$CacheFailureCopyWithImpl(
      CacheFailure _value, $Res Function(CacheFailure) _then)
      : super(_value, (v) => _then(v as CacheFailure));

  @override
  CacheFailure get _value => super._value as CacheFailure;
}

/// @nodoc
class _$CacheFailure implements CacheFailure {
  _$CacheFailure();

  bool _didmessage = false;
  String _message;

  @override
  String get message {
    if (_didmessage == false) {
      _didmessage = true;
      _message = this.map(
          networkFailure: (e) => "NetworkException",
          cacheFailure: (e) => "CacheException.");
    }
    return _message;
  }

  @override
  String toString() {
    return 'StorageFailure.cacheFailure(message: $message)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is CacheFailure);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result networkFailure(),
    @required Result cacheFailure(),
  }) {
    assert(networkFailure != null);
    assert(cacheFailure != null);
    return cacheFailure();
  }

  @override
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result networkFailure(),
    Result cacheFailure(),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (cacheFailure != null) {
      return cacheFailure();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result networkFailure(NetworkFailure value),
    @required Result cacheFailure(CacheFailure value),
  }) {
    assert(networkFailure != null);
    assert(cacheFailure != null);
    return cacheFailure(this);
  }

  @override
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result networkFailure(NetworkFailure value),
    Result cacheFailure(CacheFailure value),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (cacheFailure != null) {
      return cacheFailure(this);
    }
    return orElse();
  }
}

abstract class CacheFailure implements StorageFailure {
  factory CacheFailure() = _$CacheFailure;
}
