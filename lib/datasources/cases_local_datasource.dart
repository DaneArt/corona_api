
import 'package:corona_api/error/errors.dart';
import 'package:corona_api/models/total_cases.dart';
import 'package:hive/hive.dart';
import 'package:injectable/injectable.dart';

abstract class CovidLocalDatasource { 
  Future<TotalCases> getTotalCasesByDate(String date);
  Future<void> cacheTotalCasesByDate(TotalCases totalCases,String date);
}

@Injectable(as: CovidLocalDatasource)
class CovidLocalDatasourceImpl implements CovidLocalDatasource{

  final Box<TotalCases> hiveBox;

  @factoryMethod
  CovidLocalDatasourceImpl(@factoryParam this.hiveBox);

  @override
  Future<void> cacheTotalCasesByDate(TotalCases totalCases, String date) async {
      try{
          hiveBox.put(date, totalCases);
      }catch(e){
        throw CacheException();
      }
    }
  
    @override
    Future<TotalCases> getTotalCasesByDate(String date) async {
    try{
         return hiveBox.get(date);
      }catch(e){
        throw CacheException();
      }
  }

}
