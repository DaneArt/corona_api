// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'covid_network_datasource.dart';

// **************************************************************************
// RetrofitGenerator
// **************************************************************************

class _CovidNetworkDatasource implements CovidNetworkDatasource {
  _CovidNetworkDatasource(this._dio, {this.baseUrl}) {
    ArgumentError.checkNotNull(_dio, '_dio');
    baseUrl ??= 'https://covid-api.com/api/';
  }

  final Dio _dio;

  String baseUrl;

  @override
  Future<TotalCases> getTotalCasesByDate(date) async {
    ArgumentError.checkNotNull(date, 'date');
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{r'date': date};
    final _data = <String, dynamic>{};
    final _result = await _dio.request<Map<String, dynamic>>('/reports/total',
        queryParameters: queryParameters,
        options: RequestOptions(
            method: 'GET',
            headers: <String, dynamic>{},
            extra: _extra,
            baseUrl: baseUrl),
        data: _data);
    final value = TotalCases.fromJson(_result.data);
    return value;
  }
}
