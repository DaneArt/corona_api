import 'package:corona_api/models/total_cases.dart';
import 'package:dio/dio.dart';
import 'package:injectable/injectable.dart';
import 'package:retrofit/http.dart';

part 'covid_network_datasource.g.dart';

@RestApi(baseUrl: 'https://covid-api.com/api/')
abstract class CovidNetworkDatasource { 
  factory CovidNetworkDatasource(Dio dio, { String baseUrl}) = _CovidNetworkDatasource;

  @GET('/reports/total')
  Future<TotalCases> getTotalCasesByDate(@Query("date") String date);
}

