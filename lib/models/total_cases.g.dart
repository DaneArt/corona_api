// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'total_cases.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_TotalCases _$_$_TotalCasesFromJson(Map<String, dynamic> json) {
  return _$_TotalCases(
    date: const DateConverter().fromJson(json['date'] as String),
    confirmed: json['confirmed'] as int,
    deaths: json['deaths'] as int,
    recovered: json['recovered'] as int,
  );
}

Map<String, dynamic> _$_$_TotalCasesToJson(_$_TotalCases instance) =>
    <String, dynamic>{
      'date': const DateConverter().toJson(instance.date),
      'confirmed': instance.confirmed,
      'deaths': instance.deaths,
      'recovered': instance.recovered,
    };
