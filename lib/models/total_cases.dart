import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:hive/hive.dart';

part 'total_cases.freezed.dart';
part 'total_cases.g.dart';

@freezed
@HiveType(typeId: 0)
abstract class TotalCases implements _$TotalCases {
  const TotalCases._();

  const factory TotalCases({
    @DateConverter() @HiveField(0) @required  DateTime date,
    @HiveField(1) @required int confirmed,
    @HiveField(2) @required int deaths,
    @HiveField(3) @required int recovered,
  }) = _TotalCases;

  factory TotalCases.fromJson(Map<String, dynamic> json) =>
       _$TotalCasesFromJson(json['data']);
}


class DateConverter implements JsonConverter<DateTime,String>{
  const DateConverter();
  @override
  DateTime fromJson(String json)=>DateTime.parse(json);
    
  
    @override
    String toJson(DateTime object) => object.toString();
  
}