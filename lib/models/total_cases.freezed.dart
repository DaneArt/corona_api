// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies

part of 'total_cases.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;
TotalCases _$TotalCasesFromJson(Map<String, dynamic> json) {
  return _TotalCases.fromJson(json);
}

/// @nodoc
class _$TotalCasesTearOff {
  const _$TotalCasesTearOff();

// ignore: unused_element
  _TotalCases call(
      {@required @DateConverter() @HiveField(0) DateTime date,
      @required @HiveField(1) int confirmed,
      @required @HiveField(2) int deaths,
      @required @HiveField(3) int recovered}) {
    return _TotalCases(
      date: date,
      confirmed: confirmed,
      deaths: deaths,
      recovered: recovered,
    );
  }

// ignore: unused_element
  TotalCases fromJson(Map<String, Object> json) {
    return TotalCases.fromJson(json);
  }
}

/// @nodoc
// ignore: unused_element
const $TotalCases = _$TotalCasesTearOff();

/// @nodoc
mixin _$TotalCases {
  @DateConverter()
  @HiveField(0)
  DateTime get date;
  @HiveField(1)
  int get confirmed;
  @HiveField(2)
  int get deaths;
  @HiveField(3)
  int get recovered;

  Map<String, dynamic> toJson();
  $TotalCasesCopyWith<TotalCases> get copyWith;
}

/// @nodoc
abstract class $TotalCasesCopyWith<$Res> {
  factory $TotalCasesCopyWith(
          TotalCases value, $Res Function(TotalCases) then) =
      _$TotalCasesCopyWithImpl<$Res>;
  $Res call(
      {@DateConverter() @HiveField(0) DateTime date,
      @HiveField(1) int confirmed,
      @HiveField(2) int deaths,
      @HiveField(3) int recovered});
}

/// @nodoc
class _$TotalCasesCopyWithImpl<$Res> implements $TotalCasesCopyWith<$Res> {
  _$TotalCasesCopyWithImpl(this._value, this._then);

  final TotalCases _value;
  // ignore: unused_field
  final $Res Function(TotalCases) _then;

  @override
  $Res call({
    Object date = freezed,
    Object confirmed = freezed,
    Object deaths = freezed,
    Object recovered = freezed,
  }) {
    return _then(_value.copyWith(
      date: date == freezed ? _value.date : date as DateTime,
      confirmed: confirmed == freezed ? _value.confirmed : confirmed as int,
      deaths: deaths == freezed ? _value.deaths : deaths as int,
      recovered: recovered == freezed ? _value.recovered : recovered as int,
    ));
  }
}

/// @nodoc
abstract class _$TotalCasesCopyWith<$Res> implements $TotalCasesCopyWith<$Res> {
  factory _$TotalCasesCopyWith(
          _TotalCases value, $Res Function(_TotalCases) then) =
      __$TotalCasesCopyWithImpl<$Res>;
  @override
  $Res call(
      {@DateConverter() @HiveField(0) DateTime date,
      @HiveField(1) int confirmed,
      @HiveField(2) int deaths,
      @HiveField(3) int recovered});
}

/// @nodoc
class __$TotalCasesCopyWithImpl<$Res> extends _$TotalCasesCopyWithImpl<$Res>
    implements _$TotalCasesCopyWith<$Res> {
  __$TotalCasesCopyWithImpl(
      _TotalCases _value, $Res Function(_TotalCases) _then)
      : super(_value, (v) => _then(v as _TotalCases));

  @override
  _TotalCases get _value => super._value as _TotalCases;

  @override
  $Res call({
    Object date = freezed,
    Object confirmed = freezed,
    Object deaths = freezed,
    Object recovered = freezed,
  }) {
    return _then(_TotalCases(
      date: date == freezed ? _value.date : date as DateTime,
      confirmed: confirmed == freezed ? _value.confirmed : confirmed as int,
      deaths: deaths == freezed ? _value.deaths : deaths as int,
      recovered: recovered == freezed ? _value.recovered : recovered as int,
    ));
  }
}

@JsonSerializable()

/// @nodoc
class _$_TotalCases extends _TotalCases {
  const _$_TotalCases(
      {@required @DateConverter() @HiveField(0) this.date,
      @required @HiveField(1) this.confirmed,
      @required @HiveField(2) this.deaths,
      @required @HiveField(3) this.recovered})
      : assert(date != null),
        assert(confirmed != null),
        assert(deaths != null),
        assert(recovered != null),
        super._();

  factory _$_TotalCases.fromJson(Map<String, dynamic> json) =>
      _$_$_TotalCasesFromJson(json);

  @override
  @DateConverter()
  @HiveField(0)
  final DateTime date;
  @override
  @HiveField(1)
  final int confirmed;
  @override
  @HiveField(2)
  final int deaths;
  @override
  @HiveField(3)
  final int recovered;

  @override
  String toString() {
    return 'TotalCases(date: $date, confirmed: $confirmed, deaths: $deaths, recovered: $recovered)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _TotalCases &&
            (identical(other.date, date) ||
                const DeepCollectionEquality().equals(other.date, date)) &&
            (identical(other.confirmed, confirmed) ||
                const DeepCollectionEquality()
                    .equals(other.confirmed, confirmed)) &&
            (identical(other.deaths, deaths) ||
                const DeepCollectionEquality().equals(other.deaths, deaths)) &&
            (identical(other.recovered, recovered) ||
                const DeepCollectionEquality()
                    .equals(other.recovered, recovered)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(date) ^
      const DeepCollectionEquality().hash(confirmed) ^
      const DeepCollectionEquality().hash(deaths) ^
      const DeepCollectionEquality().hash(recovered);

  @override
  _$TotalCasesCopyWith<_TotalCases> get copyWith =>
      __$TotalCasesCopyWithImpl<_TotalCases>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$_$_TotalCasesToJson(this);
  }
}

abstract class _TotalCases extends TotalCases {
  const _TotalCases._() : super._();
  const factory _TotalCases(
      {@required @DateConverter() @HiveField(0) DateTime date,
      @required @HiveField(1) int confirmed,
      @required @HiveField(2) int deaths,
      @required @HiveField(3) int recovered}) = _$_TotalCases;

  factory _TotalCases.fromJson(Map<String, dynamic> json) =
      _$_TotalCases.fromJson;

  @override
  @DateConverter()
  @HiveField(0)
  DateTime get date;
  @override
  @HiveField(1)
  int get confirmed;
  @override
  @HiveField(2)
  int get deaths;
  @override
  @HiveField(3)
  int get recovered;
  @override
  _$TotalCasesCopyWith<_TotalCases> get copyWith;
}
