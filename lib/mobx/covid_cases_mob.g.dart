// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'covid_cases_mob.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$CovidCasesMob on CovidCasesBase, Store {
  final _$stateAtom = Atom(name: 'CovidCasesBase.state');

  @override
  _CovidCasesState get state {
    _$stateAtom.reportRead();
    return super.state;
  }

  @override
  set state(_CovidCasesState value) {
    _$stateAtom.reportWrite(value, super.state, () {
      super.state = value;
    });
  }

  final _$updateByDateAsyncAction = AsyncAction('CovidCasesBase.updateByDate');

  @override
  Future<void> updateByDate([DateTime date]) {
    return _$updateByDateAsyncAction.run(() => super.updateByDate(date));
  }

  @override
  String toString() {
    return '''
state: ${state}
    ''';
  }
}
