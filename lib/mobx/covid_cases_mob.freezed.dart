// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies

part of 'covid_cases_mob.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

/// @nodoc
class _$_CovidCasesStateTearOff {
  const _$_CovidCasesStateTearOff();

// ignore: unused_element
  _LoadingCovidCasesState loading() {
    return _LoadingCovidCasesState();
  }

// ignore: unused_element
  _ErrorCovidCasesState error({@required String message}) {
    return _ErrorCovidCasesState(
      message: message,
    );
  }

// ignore: unused_element
  _ListCovidCasesState list({@required TotalCases totalCases}) {
    return _ListCovidCasesState(
      totalCases: totalCases,
    );
  }
}

/// @nodoc
// ignore: unused_element
const _$CovidCasesState = _$_CovidCasesStateTearOff();

/// @nodoc
mixin _$_CovidCasesState {
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result loading(),
    @required Result error(String message),
    @required Result list(TotalCases totalCases),
  });
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result loading(),
    Result error(String message),
    Result list(TotalCases totalCases),
    @required Result orElse(),
  });
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result loading(_LoadingCovidCasesState value),
    @required Result error(_ErrorCovidCasesState value),
    @required Result list(_ListCovidCasesState value),
  });
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result loading(_LoadingCovidCasesState value),
    Result error(_ErrorCovidCasesState value),
    Result list(_ListCovidCasesState value),
    @required Result orElse(),
  });
}

/// @nodoc
abstract class _$CovidCasesStateCopyWith<$Res> {
  factory _$CovidCasesStateCopyWith(
          _CovidCasesState value, $Res Function(_CovidCasesState) then) =
      __$CovidCasesStateCopyWithImpl<$Res>;
}

/// @nodoc
class __$CovidCasesStateCopyWithImpl<$Res>
    implements _$CovidCasesStateCopyWith<$Res> {
  __$CovidCasesStateCopyWithImpl(this._value, this._then);

  final _CovidCasesState _value;
  // ignore: unused_field
  final $Res Function(_CovidCasesState) _then;
}

/// @nodoc
abstract class _$LoadingCovidCasesStateCopyWith<$Res> {
  factory _$LoadingCovidCasesStateCopyWith(_LoadingCovidCasesState value,
          $Res Function(_LoadingCovidCasesState) then) =
      __$LoadingCovidCasesStateCopyWithImpl<$Res>;
}

/// @nodoc
class __$LoadingCovidCasesStateCopyWithImpl<$Res>
    extends __$CovidCasesStateCopyWithImpl<$Res>
    implements _$LoadingCovidCasesStateCopyWith<$Res> {
  __$LoadingCovidCasesStateCopyWithImpl(_LoadingCovidCasesState _value,
      $Res Function(_LoadingCovidCasesState) _then)
      : super(_value, (v) => _then(v as _LoadingCovidCasesState));

  @override
  _LoadingCovidCasesState get _value => super._value as _LoadingCovidCasesState;
}

/// @nodoc
class _$_LoadingCovidCasesState implements _LoadingCovidCasesState {
  _$_LoadingCovidCasesState();

  @override
  String toString() {
    return '_CovidCasesState.loading()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is _LoadingCovidCasesState);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result loading(),
    @required Result error(String message),
    @required Result list(TotalCases totalCases),
  }) {
    assert(loading != null);
    assert(error != null);
    assert(list != null);
    return loading();
  }

  @override
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result loading(),
    Result error(String message),
    Result list(TotalCases totalCases),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (loading != null) {
      return loading();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result loading(_LoadingCovidCasesState value),
    @required Result error(_ErrorCovidCasesState value),
    @required Result list(_ListCovidCasesState value),
  }) {
    assert(loading != null);
    assert(error != null);
    assert(list != null);
    return loading(this);
  }

  @override
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result loading(_LoadingCovidCasesState value),
    Result error(_ErrorCovidCasesState value),
    Result list(_ListCovidCasesState value),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (loading != null) {
      return loading(this);
    }
    return orElse();
  }
}

abstract class _LoadingCovidCasesState implements _CovidCasesState {
  factory _LoadingCovidCasesState() = _$_LoadingCovidCasesState;
}

/// @nodoc
abstract class _$ErrorCovidCasesStateCopyWith<$Res> {
  factory _$ErrorCovidCasesStateCopyWith(_ErrorCovidCasesState value,
          $Res Function(_ErrorCovidCasesState) then) =
      __$ErrorCovidCasesStateCopyWithImpl<$Res>;
  $Res call({String message});
}

/// @nodoc
class __$ErrorCovidCasesStateCopyWithImpl<$Res>
    extends __$CovidCasesStateCopyWithImpl<$Res>
    implements _$ErrorCovidCasesStateCopyWith<$Res> {
  __$ErrorCovidCasesStateCopyWithImpl(
      _ErrorCovidCasesState _value, $Res Function(_ErrorCovidCasesState) _then)
      : super(_value, (v) => _then(v as _ErrorCovidCasesState));

  @override
  _ErrorCovidCasesState get _value => super._value as _ErrorCovidCasesState;

  @override
  $Res call({
    Object message = freezed,
  }) {
    return _then(_ErrorCovidCasesState(
      message: message == freezed ? _value.message : message as String,
    ));
  }
}

/// @nodoc
class _$_ErrorCovidCasesState implements _ErrorCovidCasesState {
  _$_ErrorCovidCasesState({@required this.message}) : assert(message != null);

  @override
  final String message;

  @override
  String toString() {
    return '_CovidCasesState.error(message: $message)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _ErrorCovidCasesState &&
            (identical(other.message, message) ||
                const DeepCollectionEquality().equals(other.message, message)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(message);

  @override
  _$ErrorCovidCasesStateCopyWith<_ErrorCovidCasesState> get copyWith =>
      __$ErrorCovidCasesStateCopyWithImpl<_ErrorCovidCasesState>(
          this, _$identity);

  @override
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result loading(),
    @required Result error(String message),
    @required Result list(TotalCases totalCases),
  }) {
    assert(loading != null);
    assert(error != null);
    assert(list != null);
    return error(message);
  }

  @override
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result loading(),
    Result error(String message),
    Result list(TotalCases totalCases),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (error != null) {
      return error(message);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result loading(_LoadingCovidCasesState value),
    @required Result error(_ErrorCovidCasesState value),
    @required Result list(_ListCovidCasesState value),
  }) {
    assert(loading != null);
    assert(error != null);
    assert(list != null);
    return error(this);
  }

  @override
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result loading(_LoadingCovidCasesState value),
    Result error(_ErrorCovidCasesState value),
    Result list(_ListCovidCasesState value),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (error != null) {
      return error(this);
    }
    return orElse();
  }
}

abstract class _ErrorCovidCasesState implements _CovidCasesState {
  factory _ErrorCovidCasesState({@required String message}) =
      _$_ErrorCovidCasesState;

  String get message;
  _$ErrorCovidCasesStateCopyWith<_ErrorCovidCasesState> get copyWith;
}

/// @nodoc
abstract class _$ListCovidCasesStateCopyWith<$Res> {
  factory _$ListCovidCasesStateCopyWith(_ListCovidCasesState value,
          $Res Function(_ListCovidCasesState) then) =
      __$ListCovidCasesStateCopyWithImpl<$Res>;
  $Res call({TotalCases totalCases});

  $TotalCasesCopyWith<$Res> get totalCases;
}

/// @nodoc
class __$ListCovidCasesStateCopyWithImpl<$Res>
    extends __$CovidCasesStateCopyWithImpl<$Res>
    implements _$ListCovidCasesStateCopyWith<$Res> {
  __$ListCovidCasesStateCopyWithImpl(
      _ListCovidCasesState _value, $Res Function(_ListCovidCasesState) _then)
      : super(_value, (v) => _then(v as _ListCovidCasesState));

  @override
  _ListCovidCasesState get _value => super._value as _ListCovidCasesState;

  @override
  $Res call({
    Object totalCases = freezed,
  }) {
    return _then(_ListCovidCasesState(
      totalCases:
          totalCases == freezed ? _value.totalCases : totalCases as TotalCases,
    ));
  }

  @override
  $TotalCasesCopyWith<$Res> get totalCases {
    if (_value.totalCases == null) {
      return null;
    }
    return $TotalCasesCopyWith<$Res>(_value.totalCases, (value) {
      return _then(_value.copyWith(totalCases: value));
    });
  }
}

/// @nodoc
class _$_ListCovidCasesState implements _ListCovidCasesState {
  _$_ListCovidCasesState({@required this.totalCases})
      : assert(totalCases != null);

  @override
  final TotalCases totalCases;

  @override
  String toString() {
    return '_CovidCasesState.list(totalCases: $totalCases)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _ListCovidCasesState &&
            (identical(other.totalCases, totalCases) ||
                const DeepCollectionEquality()
                    .equals(other.totalCases, totalCases)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(totalCases);

  @override
  _$ListCovidCasesStateCopyWith<_ListCovidCasesState> get copyWith =>
      __$ListCovidCasesStateCopyWithImpl<_ListCovidCasesState>(
          this, _$identity);

  @override
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result loading(),
    @required Result error(String message),
    @required Result list(TotalCases totalCases),
  }) {
    assert(loading != null);
    assert(error != null);
    assert(list != null);
    return list(totalCases);
  }

  @override
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result loading(),
    Result error(String message),
    Result list(TotalCases totalCases),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (list != null) {
      return list(totalCases);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result loading(_LoadingCovidCasesState value),
    @required Result error(_ErrorCovidCasesState value),
    @required Result list(_ListCovidCasesState value),
  }) {
    assert(loading != null);
    assert(error != null);
    assert(list != null);
    return list(this);
  }

  @override
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result loading(_LoadingCovidCasesState value),
    Result error(_ErrorCovidCasesState value),
    Result list(_ListCovidCasesState value),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (list != null) {
      return list(this);
    }
    return orElse();
  }
}

abstract class _ListCovidCasesState implements _CovidCasesState {
  factory _ListCovidCasesState({@required TotalCases totalCases}) =
      _$_ListCovidCasesState;

  TotalCases get totalCases;
  _$ListCovidCasesStateCopyWith<_ListCovidCasesState> get copyWith;
}
