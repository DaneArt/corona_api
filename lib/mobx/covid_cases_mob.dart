import 'package:corona_api/models/total_cases.dart';
import 'package:corona_api/usecases/update_total_info_usecase.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:mobx/mobx.dart';

part 'covid_cases_mob.g.dart';
part 'covid_cases_mob.freezed.dart';

class CovidCasesMob = CovidCasesBase with _$CovidCasesMob;

abstract class CovidCasesBase with Store {
  final UpdateTotalInfoUsecase _updateTotalInfoUsecase;

 @observable
 _CovidCasesState state = _CovidCasesState.loading();

  CovidCasesBase(this._updateTotalInfoUsecase){
    updateByDate();
  }

  @action
  Future<void> updateByDate([DateTime date]) async {
    date ??= DateTime.now();
   state = _CovidCasesState.loading();

    final result = await _updateTotalInfoUsecase(Params(date));
   
    result.fold((l) {
    state = _CovidCasesState.error(message: l.message);
    }, (r) {
     state = _CovidCasesState.list(totalCases: r);
    });
  }
}

@freezed
abstract class _CovidCasesState with _$_CovidCasesState{
  factory _CovidCasesState.loading() = _LoadingCovidCasesState;
  factory _CovidCasesState.error({@required String message}) = _ErrorCovidCasesState;
  factory _CovidCasesState.list({@required TotalCases totalCases}) = _ListCovidCasesState;
}
