import 'package:corona_api/datasources/cases_local_datasource.dart';
import 'package:corona_api/datasources/covid_network_datasource.dart';
import 'package:corona_api/error/errors.dart';
import 'package:corona_api/error/failures.dart';
import 'package:corona_api/models/total_cases.dart';
import 'package:corona_api/utils/network_connection.dart';
import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';
import 'package:intl/intl.dart';

abstract class CovidRepository {
  Future<Either<StorageFailure, TotalCases>> getTotalCovidForDate(
      DateTime date);
}

@Injectable(as: CovidRepository)
class CovidRepositoryImpl implements CovidRepository {
  final CovidNetworkDatasource _covidNetworkDatasource;
  final CovidLocalDatasource _covidLocalDatasource;
  final NetworkConnection _networkConnection;

  CovidRepositoryImpl(this._covidNetworkDatasource, this._covidLocalDatasource,
      this._networkConnection);

  @override
  Future<Either<StorageFailure, TotalCases>> getTotalCovidForDate(
      DateTime date) async {
    try {
      final formatedDate = DateFormat('yyyy-dd-MM').format(date);
      if(await _networkConnection.isConnected){
        
        final result =await _covidNetworkDatasource.getTotalCasesByDate(formatedDate);
        _covidLocalDatasource.cacheTotalCasesByDate(result, formatedDate);
        return right(result);
      }else{
        final result = await _covidLocalDatasource.getTotalCasesByDate(formatedDate);
        return right(result);
      }
    } on NetworkException {
      return left(StorageFailure.networkFailure());
    } on CacheException {
      return left(StorageFailure.cacheFailure());
    }
  }
}
