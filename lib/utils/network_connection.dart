import 'package:connection_verify/connection_verify.dart';
import 'package:injectable/injectable.dart';

abstract class NetworkConnection{
  Future<bool> get isConnected;
}

@Injectable(as: NetworkConnection)
class NetworkConnectionIml implements NetworkConnection{
  @override
  
  Future<bool> get isConnected => ConnectionVerify.connectionStatus();

}