// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// InjectableConfigGenerator
// **************************************************************************

import 'package:hive/hive.dart';
import 'package:get_it/get_it.dart';
import 'package:injectable/injectable.dart';

import '../datasources/cases_local_datasource.dart';
import '../datasources/covid_network_datasource.dart';
import '../repositories/covid_repository.dart';
import 'network_connection.dart';
import '../models/total_cases.dart';
import '../usecases/update_total_info_usecase.dart';

/// adds generated dependencies
/// to the provided [GetIt] instance

GetIt $initGetIt(
  GetIt get, {
  String environment,
  EnvironmentFilter environmentFilter,
}) {
  final gh = GetItHelper(get, environment, environmentFilter);
  gh.factoryParam<CovidLocalDatasource, Box<TotalCases>, dynamic>(
      (hiveBox, _) => CovidLocalDatasourceImpl(hiveBox));
  gh.factory<NetworkConnection>(() => NetworkConnectionIml());
  gh.factory<CovidRepository>(() => CovidRepositoryImpl(
        get<CovidNetworkDatasource>(),
        get<CovidLocalDatasource>(),
        get<NetworkConnection>(),
      ));
  gh.factory<UpdateTotalInfoUsecase>(
      () => UpdateTotalInfoUsecase(get<CovidRepository>()));
  return get;
}
