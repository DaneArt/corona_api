import 'package:corona_api/datasources/covid_network_datasource.dart';
import 'package:corona_api/mobx/covid_cases_mob.dart';
import 'package:corona_api/models/total_cases.dart';
import 'package:dio/dio.dart';
import 'package:get_it/get_it.dart';
import 'package:hive/hive.dart';
import 'package:injectable/injectable.dart';

import 'dependency_injection.config.dart';

final getIt = GetIt.instance;

@InjectableInit(
  initializerName: r'$initGetIt', 
)
void configureDependencies() {
  getIt.registerFactory<Dio>(() => Dio());
  getIt.registerFactory<CovidNetworkDatasource>(() => CovidNetworkDatasource(getIt()));
  getIt.registerFactory<CovidCasesMob>(() => CovidCasesMob(getIt()));
  getIt.registerFactory<Box<TotalCases>>(()=>Hive.box('cases_storage'));
  $initGetIt(getIt);
}
