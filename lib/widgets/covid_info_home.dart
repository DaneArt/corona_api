import 'package:corona_api/widgets/total_covid_widget.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class CovidInfoHome extends StatelessWidget {
  const CovidInfoHome({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Информация по России                   ${DateFormat.MMMd().format(DateTime.now())}"),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
         TotalCovidWidget(),
        ],
      ),
    );
  }

  
}
