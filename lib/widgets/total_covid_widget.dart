import 'package:corona_api/mobx/covid_cases_mob.dart';
import 'package:corona_api/utils/dependency_injection.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';

class TotalCovidWidget extends StatelessWidget {
  final CovidCasesMob _covidCases = getIt.get<CovidCasesMob>();

  TotalCovidWidget({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Observer(
      builder: (BuildContext context) => _covidCases.state.map(
        loading: (s) => Center(
          child: CircularProgressIndicator(),
        ),
        error: (s) => Column(
          children: [
            Text(s.message),
            RaisedButton(onPressed: () {
              _covidCases.updateByDate();
            })
          ],
        ),
        list: (s) => Column(
          children: [
            _buildTotalCovidTile(
                context, Colors.red, 'Confirmed', s.totalCases.confirmed),
            _buildTotalCovidTile(
                context, Colors.green, 'Recovered', s.totalCases.recovered),
            _buildTotalCovidTile(
                context, Colors.black, 'Dead', s.totalCases.deaths),
          ],
        ),
      ),
    );
  }

  Widget _buildTotalCovidTile(
          BuildContext context, Color color, String title, int cases) =>
      Card(
        margin: const EdgeInsets.all(16.0),
        child: ListTile(
          leading: Container(
            height: 64,
            width: 56,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(32)),
              color: color,
            ),
          ),
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(16))),
          title: Text(
            title,
            style: TextStyle(
                fontWeight: FontWeight.normal,
                fontSize: 24 * MediaQuery.of(context).textScaleFactor),
          ),
          contentPadding: const EdgeInsets.all(8),
          trailing: Text(
            cases.toString(),
            style: TextStyle(
              color: Colors.blue,
                fontWeight: FontWeight.bold,
                fontSize: 20 * MediaQuery.of(context).textScaleFactor),
          ),
        ),
      );
}
