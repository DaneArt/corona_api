

import 'package:corona_api/error/failures.dart';
import 'package:corona_api/models/total_cases.dart';
import 'package:corona_api/repositories/covid_repository.dart';
import 'package:corona_api/usecases/usecase.dart';
import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';

@Injectable()
class UpdateTotalInfoUsecase implements UseCase<StorageFailure,TotalCases,Params>{

  final CovidRepository _covidRepository;

  UpdateTotalInfoUsecase(this._covidRepository);

  @override
  Future<Either<StorageFailure,TotalCases>> call(Params params) {
    return _covidRepository.getTotalCovidForDate(params.date);
  }
}

class Params{
  final DateTime date;

  Params(this.date);
}